# This file contains the function used on Cloud Fucntions to transfer data from GCS to BigQuery, triggered by the upload of a file on the boticario bucket.
import pandas as pd
from pandas.io import gbq
from google.cloud import bigquery
from datetime import datetime
import pandas_gbq

def funcWriteGcsToBigQuery(event, context):
    """Triggered by a change to a Cloud Storage bucket.
    Args:
         event (dict): Event payload.
         context (google.cloud.functions.Context): Metadata for the event.
    """

    ssObjects = []
    sFileName = event['name']
    
    # Actual file data , writing to Big Query
    # Check whether the entry is a csv or xlsx file
    if sFileName.endswith('.csv'):
      dfData = pd.read_csv('gs://' + event['bucket'] + '/' + sFileName)
    elif sFileName.endswith('.xlsx'):
      dfData = pd.read_excel('gs://' + event['bucket'] + '/' + sFileName)

    # Converting all string columns to lower case so information will be standardized
    dfData = dfData.applymap(lambda x: x.lower() if isinstance(x, str) else x)
    # to avoid problems with simple col names on layouts
    dfData.columns= dfData.columns.str.lower()

    # The correct order of the columns, the same on the layout on BigQuery
    ssOriginalCols = ['id_marca',	'marca', 'id_linha', 'linha', 'data_venda', 'qtd_venda']

    ssDfCols = dfData.columns
    if len(ssDfCols) < len(ssOriginalCols):
      raise ValueError('Number of columns are incorrect, expected {0}, given {1}'.format(len(ssOriginalCols), len(ssDfCols)))
    
    try:
      dfData = dfData[ssOriginalCols]
    except:
      raise ValueError('Column names are incorrect, the following have wrong names {0}'.format(set(ssOriginalCols) - set(ssDfCols)))

    # change the col names to english to be standard BQ
    dfData.columns = ['id_brand', 'brand', 'id_product_line', 'product_line', 'date_sale', 'quantity']	
    # record of the date the files are being uploaded
    dfData['date_modified'] = datetime.today().strftime('%Y-%m-%d %H:%M:%S')
    dfData['event_id'] = context.event_id
    # unique identifier for each row, there wasn't any deduplication so it might appear more than once.
    dfData['uuid'] = dfData['id_brand'].astype(str) + dfData['brand'].str[0] + dfData['id_product_line'].astype(str) + dfData['product_line'].str[0] + dfData['date_sale'].astype(str).str.replace('-','') + dfData['quantity'].astype(str)
    
    # upload the data to BQ
    # The DF is converted to STR because BQ can't handle datetime fields on the parquet conversion, this is done by BigQuwery itself, based on the table schema
    pandas_gbq.to_gbq(
      dataframe = dfData.astype(str),
      destination_table = 'boticario_opr.opr', 
      project_id='boticario-proj', 
      if_exists='append',
      api_method = "load_csv",
      location='us')

    # Metadata details writing into Big Query
    dictMetadata = {
      'event_id':context.event_id,
      'event_type':context.event_type,
      'bucket_name':event['bucket'],
      'file_name':event['name'],
      'date_created':event['timeCreated'],
      'date_updated':event['updated']
      }

    ssObjects.append(dictMetadata)
    dfMetadata = pd.DataFrame.from_records(ssObjects).astype(str)
    pandas_gbq.to_gbq(
      dataframe = dfMetadata,
      destination_table = 'boticario_opr.opr_metadata', 
      project_id='boticario-proj', 
      if_exists='append',
      api_method = "load_csv",
      location='us')
