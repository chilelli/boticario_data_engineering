# Boticario Project

This repository contains scripts related to the test required from Boticario in order to fill a Data Engineer position.
It's separated in 3 folders.

* CloudFunctions: Contain the functions and requirements to execute a Google Cloud Function which transfers data from Google Cloud Storage to a BigQuery table, triggered by the upload of a file into a specific bucket.
* DataReport: Contain the file (used on the test) report, with data distribution, missing and completeness of fields.
* SQL: The queries used to create 4 summary tables for the test, with counts and sums of data. these queries run on scheduled inside BigQuery itself.

PS: To open the .html files on DataReports it is necessary to download them.